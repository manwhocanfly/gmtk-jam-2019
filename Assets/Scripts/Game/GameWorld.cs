﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class GameWorld
{
    public const int CHUNK_SIZE = 16;

    public const int SIZE_X = 8;
    public const int SIZE_Y = 8;
    public const int SIZE_Z = 4;

    public const int TILES_X = SIZE_X * CHUNK_SIZE;
    public const int TILES_Y = SIZE_Y * CHUNK_SIZE;
    public const int TILES_Z = SIZE_Z * CHUNK_SIZE;

    public const int CHUNK_COUNT = SIZE_X * SIZE_Y * SIZE_Z;
    public const int TILE_COUNT = CHUNK_COUNT * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;

    public delegate void TileChangeEvent(Vector3Int position, int tile);
    public event TileChangeEvent OnTileChange;

    public GameWorldChunk[] chunks = new GameWorldChunk[CHUNK_COUNT];

    public GameWorld()
    {
        for (int i = 0; i < CHUNK_COUNT; i++)
        {
            chunks[i] = new GameWorldChunk(ChunkIdToPosition(i));
        }
    }

    public uint tick { get; private set; }

    public bool CanChangeTile(Vector3Int position, int tile)
    {
        if (!IsInBounds(position)) return false;

        var existingTile = GetTile(position);

        if(tile != 0)
        {
            if (existingTile != 0) return false;

            var hasNeighbours = false;

            for (int i = 0; i < GameUtil.Neighbours.Length; i++)
            {
                var nTile = GetTile(position + GameUtil.Neighbours[i]);

                if (nTile != 0)
                {
                    hasNeighbours = true;
                    break;
                }
            }

            if (!hasNeighbours) return false;
        }
        else
        {
            if (existingTile == 0) return false;
        }

        return true;
    }

    public int Id(Vector3Int position)
    {
        return position.x + position.y * SIZE_X + position.z * SIZE_X * SIZE_Y;
    }

    public bool IsInBounds(Vector3Int position)
    {
        return 
            position.x >= 0 && position.x < TILES_X &&
            position.y >= 0 && position.y < TILES_Y &&
            position.z >= 0 && position.z < TILES_Z;
    }

    public int GetTile(Vector3Int position)
    {
        if (!IsInBounds(position)) return 0;

        var chunkId = ChunkPositionToId(position.x / CHUNK_SIZE, position.y / CHUNK_SIZE, position.z / CHUNK_SIZE);
        var localPosition = new Vector3Int(position.x % CHUNK_SIZE, position.y % CHUNK_SIZE, position.z % CHUNK_SIZE);

        return chunks[chunkId].GetTile(localPosition);
    }

    public bool TrySetTile(Vector3Int position, int tile, bool force = false)
    {
        if(force || CanChangeTile(position, tile))
        {
            var id = Id(position);

            var chunkId = ChunkPositionToId(position.x / CHUNK_SIZE, position.y / CHUNK_SIZE, position.z / CHUNK_SIZE);
            var localPosition = new Vector3Int(position.x % CHUNK_SIZE, position.y % CHUNK_SIZE, position.z % CHUNK_SIZE);

            chunks[chunkId].SetTile(localPosition, tile);

            OnTileChange?.Invoke(position, tile);

            for (int i = 0; i < GameUtil.Neighbours.Length; i++)
            {
                var nPos = position + GameUtil.Neighbours[i];
                OnTileChange?.Invoke(nPos, GetTile(nPos));
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public void UpdateTick()
    {
        tick++;
    }

    public Vector3Int ChunkIdToPosition(int id)
    {
        var pos = new Vector3Int
        {
            x = id % SIZE_X,
            y = (id / SIZE_X) % SIZE_Y,
            z = id / (SIZE_X * SIZE_Y)
        };

        return pos;
    }

    public int ChunkPositionToId(int x, int y, int z)
    {
        return x + y * SIZE_X + z * SIZE_X * SIZE_Y;
    }

    public void UpdateChunk(int chunkId)
    {
        var position = ChunkIdToPosition(chunkId) * CHUNK_SIZE;
        var chunk = chunks[chunkId];

        for (int x = -1; x < CHUNK_SIZE + 1; x++)
        {
            for (int y = -1; y < CHUNK_SIZE + 1; y++)
            {
                for (int z = -1; z < CHUNK_SIZE + 1; z++)
                {
                    var tilePosition = position + new Vector3Int(x, y, z);
                    if(IsInBounds(tilePosition)) OnTileChange?.Invoke(tilePosition, GetTile(tilePosition));
                }
            }
        }
    }

    public void SaveToFile()
    {
        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int unixTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

        var fileName = $"proto_{unixTime}.world";

        var bf = new BinaryFormatter();
        var fs = new FileStream($"{Application.persistentDataPath}/{fileName}", FileMode.Create);

        bf.Serialize(fs, this);

        fs.Close();
    }
}
