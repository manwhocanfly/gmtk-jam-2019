﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    void Start()
    {
        switch (QualitySettings.GetQualityLevel())
        {
            case 0:
                Camera.main.farClipPlane = 40;
                break;
            case 1:
                Camera.main.farClipPlane = 70;
                break;
            case 2:
                Camera.main.farClipPlane = 100;
                break;
            case 3:
                Camera.main.farClipPlane = 150;
                break;
            case 4:
                Camera.main.farClipPlane = 250;
                break;
            case 5:
                Camera.main.farClipPlane = 1000;
                break;
            default:
                Camera.main.farClipPlane = 1000;
                break;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F7))
        {
            QualitySettings.DecreaseLevel(true);
            Start();
        }

        if (Input.GetKeyDown(KeyCode.F8))
        {
            QualitySettings.IncreaseLevel(true);
            Start();
        }
    }
}
