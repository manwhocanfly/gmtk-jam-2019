﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChunkRenderer : MonoBehaviour
{
    public static Dictionary<int, TileRenderer> tiles = new Dictionary<int, TileRenderer>();
    Dictionary<int, Stack<TileRenderer>> pools = new Dictionary<int, Stack<TileRenderer>>();
    Dictionary<Vector3Int, TileRenderer> objects = new Dictionary<Vector3Int, TileRenderer>();
    public static Dictionary<int, bool> occlusionInfo = new Dictionary<int, bool>();

    GameWorld world;

    public List<TileRenderer> tileDefinitions = new List<TileRenderer>();

    private IEnumerator Start()
    {
        yield return null;

        for (int i = 0; i < tileDefinitions.Count; i++)
        {
            var tileDefinition = tileDefinitions[i];

            if (tileDefinition == null) continue;

            tileDefinition.type = i;

            tiles[i] = tileDefinition;
        }

        occlusionInfo = tiles.Select(x => x.Value).ToDictionary(x => x.type, x => x.occluder);

        var client = GetComponent<GameClient>();

        while (client.World == null) yield return null;

        world = client.World;

        world.OnTileChange += UpdateTile;
    }

    public TileRenderer RecycleTileObject(Vector3Int position, int type)
    {
        if (tiles.ContainsKey(type) == false) return null;

        var prefab = tiles[type];

        TileRenderer tile = null;

        if(pools.ContainsKey(type) && pools[type].Count > 0)
        {
            tile = pools[type].Pop();
        }
        else
        {
            tile = Instantiate(prefab, new Vector3(position.x, position.z, position.y), Quaternion.identity);
        }

        tile.transform.localPosition = new Vector3(position.x, position.z, position.y);
        tile.gameObject.SetActive(true);

        return tile;
    }

    public void DecycleTileObject(TileRenderer tile)
    {
        var type = tile.type;

        tile.gameObject.SetActive(false);

        if(pools.ContainsKey(type) == false)
        {
            pools[type] = new Stack<TileRenderer>();
        }

        pools[type].Push(tile);
    }

    public void UpdateTile(Vector3Int position, int type)
    {
        if(objects.ContainsKey(position))
        {
            DecycleTileObject(objects[position]);
            objects.Remove(position);
        }

        if (type == 0) return;

        occlusionInfo.TryGetValue(type, out bool isOccluder);

        bool isVisible = true;

        if (isOccluder)  // check if has all occluders neighbours 
        {
            isVisible = false;

            for (int i = 0; i < GameUtil.Neighbours.Length; i++)
            {
                var tile = world.GetTile(position + GameUtil.Neighbours[i]);

                if (!occlusionInfo.TryGetValue(tile, out bool isNeighbourOccluder) || !isNeighbourOccluder)
                {
                    isVisible = true;
                    break;
                }
            }
        }

        if(isVisible)
        {
            objects[position] = RecycleTileObject(position, type);
        }
    }
}
