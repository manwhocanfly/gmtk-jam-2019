﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float moveSpeed = 10f;
    private float rotationY = 0F;

    public GameClient client;
    public ChunkRenderer chunkRenderer;
    public Transform blockPreviewRoot;
    public TileRenderer block;
	public Transform cameraAngle;
	public Transform camera;

	public float zoom = .6f;

	private float boostTimer = 0f;

	private BlockPicker blockPicker;
	private int currentTile = 0;

	public AudioSource audioBlockPlace;
	public AudioSource audioBlockRemove;
	public Transform blockCenterPivot;
	public Transform blockContainer;

	float hideTimer = 0;

    IEnumerator Start()
    {
		blockPicker = GetComponent<BlockPicker>();
        while (client?.World == null) yield return null;
		blockPicker.GenerateList(chunkRenderer);
		SwitchBlock(0);
		blockCenterPivot.transform.localScale = Vector3.one * .9f;
    }

    private void Update()
    {
		int rotation = (Mathf.RoundToInt(transform.localEulerAngles.y / 360f * 4f) + 2) % 4;
		if(block && block.rotatable) {
			blockCenterPivot.localEulerAngles = new Vector3(0, rotation * 90, 0);
		} else {
			blockCenterPivot.localEulerAngles = Vector3.zero;
		}

        if(Input.GetButtonDown("Fire1") && block)
        {
            var blockPos = Vector3Int.RoundToInt(blockPreviewRoot.position);
            var pos = new Vector3Int(blockPos.x, blockPos.z, blockPos.y);
			
			int blockType = block.type;
			if(block.rotatable) {
				blockType += rotation;
			}

            if(client.TryPlaceTile(pos, blockType)) {
				audioBlockPlace.Play();
				blockCenterPivot.transform.localScale = Vector3.one * 1.4f;
				hideTimer = .1f;
			}
        }

        if(Input.GetButtonDown("Fire2"))
        {
            var blockPos = Vector3Int.RoundToInt(blockPreviewRoot.position);
            var pos = new Vector3Int(blockPos.x, blockPos.z, blockPos.y);

            if(client.TryPlaceTile(pos, 0)) {
				audioBlockRemove.Play();
				blockCenterPivot.transform.localScale = Vector3.zero;
				hideTimer = .1f;
				
			}
        }

        if (Input.GetButtonDown("SwitchBlockPrevious"))
        {
            SwitchBlock(-1);
        }

		if(Input.GetButtonDown("SwitchBlockNext"))
        {
            SwitchBlock(1);
        }

		if(hideTimer > 0) {
			hideTimer -= Time.deltaTime;
		} else {
			blockCenterPivot.transform.localScale += (Vector3.one * .9f - blockCenterPivot.transform.localScale) * .7f;
		}
    }

    void LateUpdate()
    {
        Cursor.lockState = CursorLockMode.Locked;

        //Rotation
        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

        if (rotationY > 90f) rotationX -= 360f;
        if (rotationY > 85f) rotationY = 85f;
        if (rotationY < -85f) rotationY = -85f;

        transform.localEulerAngles = new Vector3(0, rotationX, 0);
		cameraAngle.localEulerAngles = new Vector3(-rotationY, 0, 0);

        //Movement
        float forward = Input.GetAxis("Vertical") * moveSpeed * Time.smoothDeltaTime;
        float strafe = Input.GetAxis("Horizontal") * moveSpeed * Time.smoothDeltaTime;
        float vertical = Input.GetAxis("Jump") * moveSpeed * Time.smoothDeltaTime;

		if(new Vector3(forward, strafe, vertical).magnitude > .1f) {
			boostTimer += Time.deltaTime;
		} else {
			boostTimer = 0f;
		}

		float speed = boostTimer > .5f ? (boostTimer > 2f ? 1.5f : .8f) : .5f;

        transform.Translate(Vector3.forward * forward * speed);
        transform.Translate(Vector3.right * strafe * speed);
        transform.Translate(Vector3.up * vertical * speed);

		zoom -= Input.mouseScrollDelta.y * .1f;
		zoom = Mathf.Clamp01(zoom);
		camera.localPosition = new Vector3(0, 0, -2f - zoom * 16f);



        // Update block preview
        if(block)
        {
            var blockPosition = transform.position;

            blockPosition.x = Mathf.Round(blockPosition.x + .5f);
            blockPosition.y = Mathf.Round(blockPosition.y - .5f);
            blockPosition.z = Mathf.Round(blockPosition.z - .5f);

            blockPreviewRoot.transform.position += (blockPosition - blockPreviewRoot.transform.position) * .9f;

            blockPreviewRoot.transform.eulerAngles = Vector3.zero;
        }
    }

	void SwitchBlock(int delta) {
        if (block) Destroy(block.gameObject);

		currentTile += delta;
		if(currentTile < 0) currentTile = blockPicker.tiles.Count - 1;
		if(currentTile >= blockPicker.tiles.Count) currentTile = 0;
        block = Instantiate(chunkRenderer.tileDefinitions[blockPicker.tiles[currentTile].tile.type]);
		block.transform.SetParent(blockContainer, false);
		block.transform.localPosition = Vector3.zero;

		blockPicker.highLight(currentTile);
	}
}
