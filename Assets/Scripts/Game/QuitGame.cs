﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitGame : MonoBehaviour
{
    public CanvasGroup notification;
    public Text notificationText;

    private float lastTimePressed = -100f;
    private int timePressed;

    private int pressesRequiredToQuit = 3;
   
    void Update()
    {
        var diff = Time.time - lastTimePressed;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (diff > 3f) timePressed = pressesRequiredToQuit;
            else timePressed--;

            lastTimePressed = Time.time;

            if (timePressed == 0)
            {
                notificationText.text = "Bye!";
                Application.Quit();
            }
            else if (timePressed > 0)
            {
                notificationText.text = $"Press Esc {timePressed} more {(timePressed > 1 ? "times" : "time")} to quit!";
            }
        }

        notification.alpha = Mathf.Clamp01(3f - diff);
    }
}
