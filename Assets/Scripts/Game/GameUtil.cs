﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameUtil 
{
    public static readonly Vector3Int[] Neighbours = new Vector3Int[] { new Vector3Int(1, 0, 0), new Vector3Int(-1, 0, 0), new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0), new Vector3Int(0, 0, 1), new Vector3Int(0, 0, -1) };
}
