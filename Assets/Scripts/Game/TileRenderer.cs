﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRenderer : MonoBehaviour
{
    [HideInInspector]
    public int type = -1;

    public bool occluder = true;
	public bool rotatable = false;
}

public struct TileRendererComparer : IEqualityComparer<TileRenderer>
{
    public bool Equals(TileRenderer x, TileRenderer y)
    {
        return x.type == y.type;
    }

    public int GetHashCode(TileRenderer obj)
    {
        return obj.type.GetHashCode();
    }
}
