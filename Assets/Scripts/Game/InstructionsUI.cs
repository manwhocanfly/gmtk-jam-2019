﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsUI : MonoBehaviour
{
    public CanvasGroup group;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            group.gameObject.SetActive(!group.gameObject.activeSelf);
        }
    }
}
