﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class GameServerDB
{
    IDbConnection dbcon;
    IDbCommand dbcmd;

    public GameServerDB()
    {
        string connection = "URI=file:" + Application.persistentDataPath + "/" + "world.db";

        dbcon = new SqliteConnection(connection);
        dbcon.Open();

        Debug.Log($"[SERVER] Database connection open ({connection}).");

        dbcmd = dbcon.CreateCommand();

        string createTableQuery =
          "CREATE TABLE IF NOT EXISTS " + "changes" + " (" +
          "id" + " INTEGER PRIMARY KEY, " +
          "x" + " INTEGER, " +
          "y" + " INTEGER, " +
          "z" + " INTEGER, " +
          "type" + " INTEGER, " +
          "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP " +
          ")";

        dbcmd.CommandText = createTableQuery;

        dbcmd.ExecuteReader();
    }

    public void Push(int x, int y, int z, int type)
    {
        var cmd = dbcon.CreateCommand();
        cmd.CommandText = $"INSERT INTO changes (x,y,z,type) VALUES ({x},{y},{z},{type})";
        cmd.ExecuteNonQuery();
    }

    public void Close()
    {
        if (dbcon != null) dbcon.Close();
    }
}
