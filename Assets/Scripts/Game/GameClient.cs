using System.Net;
using System.Net.Sockets;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.Networking;

public class GameClient : MonoBehaviour, INetEventListener
{
    public static GameClient Main;

    private NetManager _client;
    private NetDataWriter _dataWriter;
    private GameWorld _world;
    private GameSettings _settings;

    public Text latencyText;
    public bool useLocalServer;
    public string serverScene = "Server";

    public GameWorld World => _world;
    public ConnectionState ConnectionState => _client.FirstPeer?.ConnectionState ?? ConnectionState.Disconnected;

    private void Awake()
    {
        Main = this;
    }

    IEnumerator Start()
    {
        var configRequest = UnityWebRequest.Get("https://noopol.com/gmtk-jam-2019/config.json");
        yield return configRequest.SendWebRequest();

        if(configRequest.isNetworkError || configRequest.isHttpError)
        {
            latencyText.text = "Server offline";
            yield break;
        }
        else
        {
            _settings = JsonUtility.FromJson<GameSettings>(configRequest.downloadHandler.text);
            if (_settings == null) yield break;
        }

        _world = new GameWorld();

        _dataWriter = new NetDataWriter();
        _client = new NetManager(this);
        _client.Start();
        _client.UpdateTime = 15;

        if(useLocalServer)
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(serverScene, UnityEngine.SceneManagement.LoadSceneMode.Additive);
        }
        else
        {
            _client.Connect(_settings.ip, GameConfig.SERVER_PORT, GameConfig.SECRET_KEY);
        }
    }

    bool saved = false;

    private void SaveWorld()
    {
        if (saved) return;

        saved = true;

        var peer  = _client.FirstPeer;

        if (peer == null || peer.ConnectionState != ConnectionState.Connected) return;

        _dataWriter.Reset();

        _dataWriter.Put((byte)2);

        _client.SendToAll(_dataWriter, DeliveryMethod.ReliableUnordered);
    }

    public bool TryPlaceTile(Vector3Int position, int tileType)
    {
        var peer = _client.FirstPeer;

        if (peer == null || peer.ConnectionState != ConnectionState.Connected) return false;
        if (World == null || !World.CanChangeTile(position, tileType)) return false;

        _dataWriter.Reset();

        _dataWriter.Put((byte)1);
        _dataWriter.Put((byte)position.x);
        _dataWriter.Put((byte)position.y);
        _dataWriter.Put((byte)position.z);

        _dataWriter.Put(tileType);

        _client.SendToAll(_dataWriter, DeliveryMethod.ReliableUnordered);

        return true;
    }

    void Update()
    {
        if (_client == null) return;

        _client.PollEvents();

        var peer = _client.FirstPeer;

        if ((peer == null || peer.ConnectionState != ConnectionState.Connected) && useLocalServer)
        {
           _client.SendDiscoveryRequest(new byte[] {1}, GameConfig.SERVER_PORT);
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F1)) SaveWorld();
#endif
    }

    void OnDestroy()
    {
        if (_client != null)
        {
            _client.Stop();
        }
    }

    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("[CLIENT] We connected to " + peer.EndPoint);
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
    {
        Debug.Log("[CLIENT] We received error " + socketErrorCode);
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        //Debug.Log($"Recieved data with {reader.AvailableBytes} bytes");

        if (reader.AvailableBytes == 0) return;

        var commandType = reader.GetByte();

        if(commandType == 1) // Place Block
        {
            if (reader.AvailableBytes < 11) return;

            var position = new Vector3Int(reader.GetByte(), reader.GetByte(), reader.GetByte());
            var tileTick = reader.GetInt();
            var tileType = reader.GetInt();

            World.TrySetTile(position, tileType, true);

            //Debug.Log($"RECEIVED BLOCK @({position}): = {tileTick}:{tileType}");
        }
        else if(commandType == 17) // Recieved whole chunk
        {
            var chunkId = _world.ChunkPositionToId(reader.GetByte(), reader.GetByte(), reader.GetByte());

            reader.GetBytes(_world.chunks[chunkId].bytes, 4, reader.AvailableBytes);

            _world.UpdateChunk(chunkId);

            //Debug.Log($"RECEIVED CHUNK {chunkId} : {_world.chunks[chunkId].GetHash()}");
        }
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryResponse && _client.PeersCount == 0)
        {
            Debug.Log("[CLIENT] Received discovery response. Connecting to: " + remoteEndPoint);
            _client.Connect(remoteEndPoint, GameConfig.SECRET_KEY);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        latencyText.text = $"Latency: {latency}ms";
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("[CLIENT] We disconnected because " + disconnectInfo.Reason);
        latencyText.text = "Not Connected";
    }
}