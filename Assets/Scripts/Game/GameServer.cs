using System.Net;
using System.Net.Sockets;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using System.Collections.Generic;
using System.Collections;

public class PeerInfo
{
    public float timePlacedTile = -100;
}


public class GameServer : MonoBehaviour, INetEventListener, INetLogger
{
    protected Dictionary<NetPeer, PeerInfo> peerInfos = new Dictionary<NetPeer, PeerInfo>();

    protected NetManager _server;
    protected NetDataWriter _dataWriter;

    protected GameWorld world;
    public bool initialized { get; private set; }

    private GameServerDB db;
	public Texture2D heightmap;
	public Texture2D treemap;
	public Texture2D sandmap;

    protected void Start()
    {
        NetDebug.Logger = this;

        world = new GameWorld();

        GenerateStartingState();

        _dataWriter = new NetDataWriter();
        _server = new NetManager(this);
        _server.Start(GameConfig.SERVER_PORT);
        _server.DiscoveryEnabled = true;
        _server.UpdateTime = 15;

        Debug.Log("[SERVER] Server running.");

#if UNITY_STANDALONE_LINUX && !UNITY_EDITOR
        db = new GameServerDB();
#endif

        initialized = true;
    }

    protected virtual void GenerateStartingState()
    {
		int tileDirt = 1;
		int tileStone = 2;
		int tileSand = 3;
		int tileSnow = 45;
		int tileGrass = 16;
		int tileFlower = 17;
		
        var center = new Vector3Int(GameWorld.TILES_X / 2, GameWorld.TILES_Y / 2, GameWorld.TILES_Z / 2);
		
		for(int x = 0; x < heightmap.width; x++) {
			for(int y = 0; y < heightmap.height; y++) {

				int height = Mathf.FloorToInt(heightmap.GetPixel(x,y).r * 30) + ((heightmap.GetPixel(x,y).r + heightmap.GetPixel(x,y).g + heightmap.GetPixel(x,y).b > 0) ? 1 : 0);
				bool tree = treemap.GetPixel(x,y).r > .5f;

				bool sand = sandmap.GetPixel(x,y).r > .5f;

				int topTile = tileDirt;
				if(sand) topTile = tileSand;


				for(int z = 0; z < height; z++) {
					world.TrySetTile(new Vector3Int(x, y, z), (z > height - Random.Range(3,6)) ? (z > Random.Range(22,25) ? tileSnow : topTile) : tileStone, true);
				}
				if(tree) {
					addTree(x,y,height);
				} else if(Random.Range(0f, 1f) < .2f && !sand) {
					if(Random.Range(0f, 1f) < .1f) {
						world.TrySetTile(new Vector3Int(x, y, height), tileFlower, true);
					} else {
						world.TrySetTile(new Vector3Int(x, y, height), tileGrass, true);
					}
				}
			}
		}
    }

    void addTree(int x, int y, int z) {
		int tileTrunk = 4;
		int tileLeaves = 5;

		int height = Random.Range(3, 6);
		for(int i = 0; i < height; i++) {
			world.TrySetTile(new Vector3Int(x, y, z + i), tileTrunk, true);
		}

		for(int lx = -2; lx < 3; lx++) {
			for(int ly = -2; ly < 3; ly++) {
				for(int lz = 0; lz < 2; lz++) {
					if((lx == -2 || lx == 2) && (ly == -2 || ly == 2) && Random.Range(0f, 1f) > .1f) continue;
					world.TrySetTile(new Vector3Int(x + lx, y + ly, z + height + lz), tileLeaves, true);
				}
			}
		}

		world.TrySetTile(new Vector3Int(x, y, z + height + 2), tileLeaves, true);
		world.TrySetTile(new Vector3Int(x - 1, y, z + height + 2), tileLeaves, true);
		world.TrySetTile(new Vector3Int(x + 1, y, z + height + 2), tileLeaves, true);
		world.TrySetTile(new Vector3Int(x, y - 1, z + height + 2), tileLeaves, true);
		world.TrySetTile(new Vector3Int(x, y + 1, z + height + 2), tileLeaves, true);
    }

    protected virtual void Update()
    {
        _server.PollEvents();
    }

    void FixedUpdate()
    {
        world.UpdateTick();
    }

    void OnDestroy()
    {
        NetDebug.Logger = null;

        if (_server != null)
        {
            _server.Stop();
        }

        if(db != null)
        {
            db.Close();
        }

#if UNITY_STANDALONE_LINUX && !UNITY_EDITOR
        world.SaveToFile();
#endif
    }

    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("[SERVER] We have new peer " + peer.EndPoint);

        peerInfos.Add(peer, new PeerInfo());

        for (int i = 0; i < world.chunks.Length; i++)
        {
            peer.Send(world.chunks[i].bytes, DeliveryMethod.ReliableUnordered);
        }
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
    {
        Debug.Log("[SERVER] error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader,
        UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryRequest)
        {
            Debug.Log("[SERVER] Received discovery request. Send discovery response");
            _server.SendDiscoveryResponse(new byte[] {1}, remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
        if(_server.PeersCount >= GameConfig.MAX_PEERS)
        {
            request.Reject();
        }
        else
        {
            request.AcceptIfKey(GameConfig.SECRET_KEY);
        }
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("[SERVER] peer disconnected " + peer.EndPoint + ", info: " + disconnectInfo.Reason);
        if (peerInfos.ContainsKey(peer)) peerInfos.Remove(peer);
    }

    public virtual void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        if (reader.AvailableBytes == 0) return;

        var commandType = reader.GetByte();

        if(commandType == 1) // Place Block
        {
            if (reader.AvailableBytes < 7) return;

            if (!peerInfos.ContainsKey(peer) || (Time.time - peerInfos[peer].timePlacedTile) < GameConfig.SET_TILE_MIN_INTERVAL)
            {
                return;
            }

            var position = new Vector3Int(reader.GetByte(), reader.GetByte(), reader.GetByte());
            var tile = reader.GetInt();

            //SetTile(position, tile);
            if (world.TrySetTile(position, tile))
            {
                _dataWriter.Reset();

                _dataWriter.Put((byte)1);
                _dataWriter.Put((byte)position.x);
                _dataWriter.Put((byte)position.y);
                _dataWriter.Put((byte)position.z);
                _dataWriter.Put(world.tick);
                _dataWriter.Put(tile);

                _server.SendToAll(_dataWriter, DeliveryMethod.ReliableUnordered);

                if (db != null) db.Push(position.x, position.y, position.z, tile);
            }
        }
        else if(commandType == 2) // Save world
        {
            world.SaveToFile();
        }
    }

    public void SetTile(Vector3Int position, int tileType)
    {
        if (world.TrySetTile(position, tileType))
        {
            _dataWriter.Reset();

            _dataWriter.Put((byte)1);
            _dataWriter.Put((byte)position.x);
            _dataWriter.Put((byte)position.y);
            _dataWriter.Put((byte)position.z);
            _dataWriter.Put(world.tick);
            _dataWriter.Put(tileType);

            _server.SendToAll(_dataWriter, DeliveryMethod.ReliableUnordered);

            if (db != null) db.Push(position.x, position.y, position.z, tileType);
        }
    }

    public void WriteNet(NetLogLevel level, string str, params object[] args)
    {
        Debug.LogFormat(str, args);
    }
}