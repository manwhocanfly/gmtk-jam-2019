﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConfig
{
    public const string SECRET_KEY = "q0RLrsgljoKoY0r2kOkK";
    public const int SERVER_PORT = 5931;
    public const int MAX_PEERS = 512;
    public const float SET_TILE_MIN_INTERVAL = 0.05f;
}

[Serializable]
public class GameSettings
{
    public string ip;
}