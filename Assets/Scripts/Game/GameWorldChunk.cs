﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameWorldChunk
{
    // starts with 4 bytes, (chunkId:17, pos.x, pos.y, pos.z)
    public byte[] bytes = new byte[4 + GameWorld.CHUNK_SIZE * GameWorld.CHUNK_SIZE * GameWorld.CHUNK_SIZE * 2];

    public GameWorldChunk(Vector3Int position)
    {
        bytes[0] = 17;
        bytes[1] = (byte)position.x;
        bytes[2] = (byte)position.y;
        bytes[3] = (byte)position.z;
    }

    public void SetTile(Vector3Int position, int type)
    {
        var id = 4 + (position.x + position.y * GameWorld.CHUNK_SIZE + position.z * GameWorld.CHUNK_SIZE * GameWorld.CHUNK_SIZE) * 2;

        bytes[id] = (byte)type;
        bytes[id + 1] = (byte)(type >> 8);
    }

    public int GetTile(Vector3Int position)
    {
        var id = 4 + (position.x + position.y * GameWorld.CHUNK_SIZE + position.z * GameWorld.CHUNK_SIZE * GameWorld.CHUNK_SIZE) * 2;
        var tile = bytes[id] | (bytes[id + 1] << 8);

        return tile;
    }

    public int GetHash()
    {
        int hash = 0;

        for (int i = 0; i < bytes.Length; i++)
        {
            unchecked { hash += bytes[i] * i; }
        }

        return hash;
    }
}
