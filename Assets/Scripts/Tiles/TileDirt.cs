﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDirt : TileAbstract
{
    public MeshFilter meshFilter;

    public Mesh meshDirt;
    public Mesh meshGrass;

    protected override void OnDraw(GameWorld world)
    {
        var topTileType = world.GetTile(Position + new Vector3Int(0, 0, 1));

        ChunkRenderer.occlusionInfo.TryGetValue(topTileType, out bool isOccluder);

        meshFilter.sharedMesh = isOccluder ? meshDirt : meshGrass;
    }
}
