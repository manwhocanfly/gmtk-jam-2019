﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileAbstract : MonoBehaviour
{
    public Vector3Int Position
    {
        get
        {
            var pos = Vector3Int.RoundToInt(transform.position);
            return new Vector3Int(pos.x, pos.z, pos.y);
        }
    }

    protected void OnEnable()
    {
        if (GameClient.Main && GameClient.Main.World != null) OnDraw(GameClient.Main.World);
    }

    protected abstract void OnDraw(GameWorld world);
}
