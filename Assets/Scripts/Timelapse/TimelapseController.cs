﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelapseController : MonoBehaviour
{
    TimelapseServer server;

    public RectTransform timeBar;

    public float duration = 60;

    bool playing = true;

    IEnumerator Start()
    {
        if (timeBar) timeBar.localScale = Vector3.zero;

        while (server == null || server.initialized == false)
        {
            server = FindObjectOfType<TimelapseServer>();
            yield return null;
        }

        var totalChanges = server.changes.Count;
        var changesPerSecond = Mathf.CeilToInt(totalChanges / duration);
        var currentChange = -1;
        
        while(true)
        {
            while (playing == false) yield return null;

            var totalChangesThisFrame = Mathf.CeilToInt(changesPerSecond * Time.deltaTime);

            for (int i = 0; i < totalChangesThisFrame; i++)
            {
                if (++currentChange >= totalChanges) yield break;

                var change = server.changes[currentChange];

                server.SetTile(change.position, change.tileType);
            }

            var timePct = Mathf.Clamp01((float)currentChange / totalChanges);

            if (timeBar) timeBar.localScale = new Vector3(timePct, 1f, 1f);

            yield return null;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) playing = !playing;
    }
}
