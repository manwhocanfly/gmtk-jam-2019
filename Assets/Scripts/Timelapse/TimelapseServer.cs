﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public struct TileChange
{
    public Vector3Int position;
    public int tileType;
}

public class TimelapseServer : GameServer
{
    private IDbConnection dbcon;
    private IDbCommand dbcmd;

    public List<TileChange> changes = new List<TileChange>();

    protected override void GenerateStartingState()
    {
        var fileName = $"{Application.dataPath}/StreamingAssets/proto.world";

        var bf = new BinaryFormatter();
        var fs = new FileStream(fileName, FileMode.Open);

        world = (GameWorld)bf.Deserialize(fs);

        fs.Close();

        string connection = "URI=file:" + Application.dataPath + "/StreamingAssets/" + "world.db";

        dbcon = new SqliteConnection(connection);
        dbcon.Open();

        Debug.Log($"[SERVER] Database connection open ({connection}).");

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = "SELECT x,y,z,type FROM changes";
        var reader = dbcmd.ExecuteReader();

        while (reader.Read())
        {
            var change = new TileChange
            {
                position = new Vector3Int(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2)),
                tileType = reader.GetInt32(3)
            };

            changes.Add(change);
        }

        dbcon.Close();
    }

    int curChange = 0;
}
