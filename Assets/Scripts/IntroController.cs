﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroController : MonoBehaviour
{
    void Start()
    {
#if UNITY_STANDALONE_LINUX
        UnityEngine.SceneManagement.SceneManager.LoadScene("Server");
#else
        UnityEngine.SceneManagement.SceneManager.LoadScene("Client");
#endif
    }
}
