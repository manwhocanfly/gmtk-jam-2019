﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPicker : MonoBehaviour
{
	public Transform pickerList;
    public List<TilePickerItem> tiles = new List<TilePickerItem>();

	private List<Transform> previews = new List<Transform>();

	private float spacing = .06f;
	private float scale = .04f;

    public void GenerateList(ChunkRenderer chunkRenderer)
    {
        for (int i = 0; i < tiles.Count; i++)
		{
			var preview = Instantiate(chunkRenderer.tileDefinitions[tiles[i].tile.type]);

			preview.transform.parent = pickerList;
			preview.transform.localScale = Vector3.one * scale;
			preview.transform.localPosition = new Vector3(i * spacing - (spacing * tiles.Count / 2f), 0, 0);
			preview.transform.localEulerAngles = new Vector3(0, 45 + 90, 0);
			previews.Add(preview.transform);

            foreach(var r in preview.GetComponentsInChildren<MeshRenderer>())
            {
                r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
		}
    }

	public void highLight(int index) {
        for (int i = 0; i < previews.Count; i++)
		{
			if(i == index)
			previews[i].transform.localScale = Vector3.one * scale * 1.5f;
			else
			previews[i].transform.localScale = Vector3.one * scale;
		}
	}

}

[System.Serializable]
public class TilePickerItem {
    public TileRenderer tile;
}